def salaire_net_cadre():
    salaire_brut_annuel = int(input("Quel est votre salaire cadre annuel brut?"))
    salaire_net_annuel = salaire_brut_annuel * 0.75
    salaire_net_mensuel = salaire_net_annuel / 12
    print(f"Votre salaire net annuel est de: {salaire_net_annuel}euros.")
    print(f"Votre salaire net mensuel est de: {salaire_net_mensuel}euros.")


def salaire_net_non_cadre():
    salaire_brut_annuel = int(input("Quel est votre salaire cadre annuel brut?"))
    salaire_net_annuel = salaire_brut_annuel * 0.78
    salaire_net_mensuel = salaire_net_annuel / 12
    print(f"Votre salaire net annuel est de: {salaire_net_annuel} euros.")
    print(f"Votre salaire net mensuel est de: {salaire_net_mensuel} euros.")


def pourcentage_augmentation():
    salaire_actuel = int(input("Entrez votre salaire actuel brut:"))
    nouveau_salaire = int(input("Entrez votre nouveau salaire brut: "))
    augmentation = (nouveau_salaire - salaire_actuel) / salaire_actuel * 100
    print(f"Le pourcentage d'augmentation est de: {augmentation:.2f}.")


menu = ""
while menu not in {"1", "2", "3", "4"}:
    menu = input(
        "| Tapez 1 - cadre | 2 - non cadre | 3 - Calcul pourcentage augmentation | 4 - quitter le programme | ")
    if menu == "1":
        salaire_net_cadre()
    elif menu == "2":
        salaire_net_non_cadre()
    elif menu == "3":
        pourcentage_augmentation()
    elif menu == "4":
        exit()
