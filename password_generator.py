import string
import secrets

print("Générateur de mots de passe. \n")

mdp = string.ascii_letters + string.digits + string.punctuation
while True:
    password = ''.join(secrets.choice(mdp) for i in range(16))
    if (any(c.islower() for c in password)
            and any(c.isupper() for c in password)
            and sum(c.isdigit() for c in password) >= 3):
        break
print("*** mot de passe généré ***")
print("***************************")
print(password)
print("***************************")
print("**** By Marmogres TEAM ****")
