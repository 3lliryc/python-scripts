import sys
menu = ""
print("Outil de calcul salaire brut/net, pourcentage d'augmentation.")
print("Outil réalisé par Marmogres Team.")
print("\n")
while menu not in {"1", "2", "3, 4 , 5"}:
    menu = input("| Tapez 1 - cadre | 2 - non cadre | 3 - Calcul pourcentage augmentation | 4 - quitter le programme | ")
    if menu == "1":
        s_brut_an = int(input("Quel est votre salaire cadre annuel brut?"))
        s_net_an = s_brut_an * 0.75
        s_net_mens = s_net_an / 12
        print(f" Votre salaire brut annuel est de: {s_brut_an} euros.")
        print(f" Votre salaire net annuel est de: {s_net_an} euros.")
        print(f" Votre salaire net mensuel est de: {s_net_mens} euros.")
        print("Merci d'avoir utilisé l'outil.")
    elif menu == "2":
        s_brut_an = int(input("Quel est votre salaire non cadre annuel brut? "))
        s_net_an = s_brut_an * 0.78
        s_net_mens = s_net_an / 12
        print(f" Votre salaire brut annuel est de: {s_brut_an} euros.")
        print(f" Votre salaire net annuel est de: {s_net_an} euros.")
        print(f" Votre salaire net mensuel est de: {s_net_mens} euros.")
        print("Merci d'avoir utilisé l'outil.")
    elif menu == "3":
        a_salaire, n_salaire = int(input("Entrez votre salaire actuel: ")), int(input("Entrez votre nouveau salaire: "))
        p_augm = (n_salaire - a_salaire) / a_salaire * 100
        p_augm = int(p_augm)
        print(f"Votre pourcentage d'augmentation est de {p_augm} %")
        print("Merci d'avoir utiliser l'outil.")
    elif menu == "4":
        print("Au revoir")  # on quitte
        sys.exit()
    else:
        ...
